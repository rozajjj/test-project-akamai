package pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.SeleniumBaseTest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainPage extends SeleniumBaseTest {

    protected WebDriver driver;

    public MainPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    private String GENERIC_CHARACTERISTIC_SORTOPTION_XPATH = "//option[contains(text(),'%s')]";

    @FindBy(id = "category")
    private WebElement inputCategory;
    @FindBy(id = "location")
    private WebElement inputLocation;
    @FindBy(xpath = "//*[@id='results']//*[contains(@class,'item')]")
    private List<WebElement> resultsList;
    @FindBy(xpath = "//*[@id='category_list']/li")
    private WebElement inputCategoryResult;
    @FindBy(xpath = "//*[@id='location_list']/li")
    private WebElement inputLocationResults;
    @FindBy(xpath = "//*[contains(@aria-labelledby,'header-location')]")
    private List<WebElement> locationResults;
    @FindBy(id = "sortbySelect")
    private WebElement buttonSort;
    @FindBy(xpath = "//*[@aria-labelledby='header-titler']/a")
    private List<WebElement> titleResults;
    @FindBy(id="keywordLocation")
    private WebElement inputKeyword;
    @FindBy(xpath = "//*[@id='keywordLocation_list']/li")
    private WebElement inputKeywordResult;

    public MainPage enterCategory(String category) {
        inputCategory.sendKeys(category);
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.textToBePresentInElement(inputCategoryResult, category));
        inputCategoryResult.click();
        return this;
    }

    public MainPage enterLocation(String location) {
        inputLocation.sendKeys(location);
        new WebDriverWait(driver, 20)
                .until(ExpectedConditions.textToBePresentInElement(inputLocationResults, location));
        inputLocationResults.click();
        return this;
    }

    public MainPage enterKeyword(String keyword){
        inputKeyword.sendKeys(keyword);
        new WebDriverWait(driver, 20)
                .until(ExpectedConditions.textToBePresentInElement(inputKeywordResult, keyword.toLowerCase()));
        inputKeywordResult.click();
        return this;
    }

    public MainPage sortByCondition(String expectedOption) {
        buttonSort.click();

        String optionXpath = String.format(GENERIC_CHARACTERISTIC_SORTOPTION_XPATH, expectedOption);
        driver.findElement(By.xpath(optionXpath)).click();
        return this;
    }

    public MainPage assertResultsNotEmpty() {
        Assert.assertFalse(resultsList.size() == 0);
        return this;
    }

    public MainPage assertCorrectLocation(String location) {
        for (WebElement locResult : locationResults) {
            Assert.assertTrue(locResult.getText().contains(location));
        }
        return this;
    }

    public MainPage assertResultsInReverse() {
        //get current list and reverse
        List<String> titles = new ArrayList<String>();
        for (WebElement title : titleResults) {
            titles.add(title.getText());
        }
        Collections.sort(titles);
        Collections.reverse(titles);

        //compare reversed list with current
        for (int i = 0; i <= titleResults.size() - 1; i++) {
            Assert.assertTrue(titleResults.get(i).getText().equals(titles.get(i)));
        }
        return this;
    }

    public MainPage assertNoResults(){
        Assert.assertTrue(resultsList.size() == 0);
        return this;
    }
}
