import org.testng.annotations.Test;
import pages.MainPage;
import utils.SeleniumBaseTest;

public class UITest extends SeleniumBaseTest {

    @Test(groups = {"UI Tests"})
    public void testCase1() {

        new MainPage(driver)
            .enterCategory("Software Engineering")
            .enterLocation("Krakow")
            .assertResultsNotEmpty()
            .assertCorrectLocation("Poland Poland_State Krakow");
    }

    @Test(groups = {"UI Tests"})
    public void testCase2(){

        new MainPage(driver)
                .enterCategory("Software Engineering")
                .enterLocation("Krakow")
                .sortByCondition("Job Title")
                .assertResultsInReverse();
    }

    @Test(groups =  {"UI Tests"})
    public void testCase3(){
        new MainPage(driver)
                .enterKeyword("This job does offer does not exist")
                .enterLocation("Krakow")
                .assertNoResults();
    }
}
