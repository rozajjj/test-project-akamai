import config.Config;
import io.restassured.http.ContentType;
import org.hamcrest.Matchers;
import org.testng.annotations.Test;
import io.restassured.RestAssured;

import java.util.HashMap;
import java.util.Map;

public class APITest {

    Config config = new Config();
    private String url = config.getApiApplicationUrl();

    @Test(groups = {"API Tests"})
    public void testCaseApi1(){
        RestAssured
                .given()
                .when()
                    .get(url)
                .then()
                    .assertThat().body("findAll.title", Matchers.notNullValue()).and().statusCode(200);
    }

    @Test(groups =  {"API Tests"})
    public void testCaseApi2(){
        RestAssured
                .given()
                .when()
                        .get(url +"/2")
                .then()
                    .assertThat().statusCode(200).and().body("id", Matchers.equalTo(2));
    }

    @Test(groups = {"API Tests"})
    public void testCaseApi3(){
        Map<String, String> map = new HashMap<String, String>();
        map.put("title", "this is title");
        map.put("body", "this is body");
        map.put("userId", "this is user id");

        RestAssured
                .given()
                    .baseUri(url)
                    .contentType(ContentType.JSON)
                    .body(map)
                .when()
                    .post()
                .then()
                    .assertThat().statusCode(201);
    }
}
